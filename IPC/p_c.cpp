#include <iostream>
#include <queue>
#include <pthread.h>
#include <semaphore.h>
using namespace std;

#define MAX_SIZE 10     /* Maximum size of the buffer. */
#define MAX_PRO 20      /* No. of items to produce. */
#define MAX_CON 20      /* No. of items to consume. */

/* Uses three semaphores full, empty and mutex. 
 * full : No. of slots which are full. Initial value = 0.
 *        Consumer can not consume any item until it is 0. So it waits on this
 *        semaphore. After each item is produced, producer signals (increments)
 *        this semaphore.
 * empty : No. of slots which are empty. Initial value = MAX_SIZE.
 *         Producer can not produce any item until it is full (= MAX_SIZE). So
 *         it waits on this semaphore. After consuming each item consumer
 *         signals this semaphore.
 * mutex : Binary semaphore to provide mutual exclusion between the producer
 *         and consumer threads. With the help of this, every operation on the
 *         buffer (q) is atomic in nature. */

int produced = 0;                 /* No. of items produced so far. */
int consumed = 0;                 /* No. of items consumed so far. */
sem_t full, empty, mutex;         /* Semaphores. */
queue<int> q;                     /* Buffer. */

void *producer (void *ptr)
{
  while (1)
  {
    sem_wait (&empty);          
    sem_wait (&mutex);
    q.push (produced++);                     /* Push the item to the queue. */
    cout << "Produced " << produced << endl;
    sem_post (&mutex);
    sem_post (&full);
    if (produced == MAX_PRO)
      break;
  }
  return NULL;
}

void *consumer (void *ptr)
{
  while (1)
  {
    sem_wait (&full);
    sem_wait (&mutex);
    int value = q.front ();           /* Get the front element of the buffer. */
    q.pop ();                         /* Remove that element. */
    cout << "Consumed " << value + 1 << endl;
    consumed++;
    sem_post (&mutex);
    sem_post (&empty);
    if (consumed == MAX_CON)
      break;
  }
  return NULL;
}

int main ()
{
  int id1, id2;                     /* Thread ids. */
  pthread_t prod, cons;             /* Threads for producer and consumer. */

  /* Initialize the semaphores. */
  sem_init (&empty, 0, MAX_SIZE);
  sem_init (&full, 0, 0);
  sem_init (&mutex, 0, 1);

  /* Create threads, specifying their corresponding functions. */
  id1 = pthread_create (&prod, NULL, producer, NULL);
  id2 = pthread_create (&cons, NULL, consumer, NULL);

  /* Wait for producer and consumer threads to complete. It doesn't guarantee
   * the order of execution of these threads, it only guarantees that the main 
   * thread won't finish until these threads have finished. */
  pthread_join (prod, NULL);
  pthread_join (cons, NULL);

  return 0;
}
