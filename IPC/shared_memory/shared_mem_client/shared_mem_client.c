#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
void main ()
{
  key_t key = 2341;     // Memory segment with this key.
  char *shm;
  int shmid;
  int segment_size = 4 * 1024;

  /* Locate the memory segment. */
  shmid = shmget (key, segment_size, 0666);
  if (shmid < 0)
  {
    printf ("ERROR SHMGET\n");
    return;
  }
  
  /* Attach the memory segment to this process (client). */
  shm = shmat (shmid, 0, 0);

  /* Read the contents of the shared memory. */
  printf ("Reading from the shared memory %s\n", shm);

  /* Update the first character to send an acknowledgement. */
  *shm = '*';

  /* Detach the shared memory segment. */
  shmdt (shm);
}
