#include <bits/stdc++.h>
using namespace std;

vector<int> result;

/* */
int sstf (vector<int>& a, vector<int>::iterator l, vector<int>::iterator r, 
          vector<int>::iterator cur)
{ 
  if (l == a.end () && r == a.end ())
    return 0;  

  int left_diff = (l == a.end ()) ? INT_MAX : abs (*l - *cur);
  int right_diff = (r == a.end ()) ? INT_MAX : abs (*r - *cur);
  if (left_diff < right_diff)
  {
    result.push_back (*l);
    cur = l;
    if (l != a.begin ())
      l--;
    else
      l = a.end ();
    return left_diff + sstf (a, l, r, cur);  
  }
  else
  {
    result.push_back (*r);
    cur = r;
    r++;
    return right_diff + sstf (a, l, r, cur);
  }
}

int main ()
{
  int n, x;
  cout << "Enter number of disk requests\n";
  cin >> n;
  vector<int> a (n + 1);
  cout << "Enter the request queue\n";
  for (int i = 0; i < n; i++)
    cin >> a[i];
  cout << "Enter current head position\n";
  cin >> x;
  a[n] = x;
  sort (a.begin (), a.end ());
  for (auto j = a.begin (); j != a.end (); j++)
    cout << (*j) << "  ";
  cout << endl;
  int cur = 0;
  for (int i = 0; i < n + 1; i++)
  {
    if (a[i] == x)
    {
      cur = i;
      break;
    }
  }
  vector<int>::iterator left = (cur == 0) ? a.end () : (a.begin () + cur - 1);
  vector<int>::iterator right = (cur == n) ? a.end () : (a.begin () + cur + 1);
  cout << sstf (a, left, right, a.begin () + cur) << endl;
  for (auto j = result.begin (); j != result.end (); j++)
    cout << (*j) << "  ";
  cout << endl;
  return 0;
}

