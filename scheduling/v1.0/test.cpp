#include "fcfs.h"
#include "sjf.h"
#include "process.h"
#include <bits/stdc++.h>

using namespace std;

void p1_task ()
{
	cout << "Inside p1\n";
}
void p2_task ()
{
	cout << "Inside p2\n";
}
void p3_task ()
{
	cout << "Inside p3\n";
}

int main ()
{
	string p1_n = "p1", p2_n = "p2", p3_n = "p3";
	process p1 (p1_n, 1, 0, 0, 24, p1_task);
	process p2 (p2_n, 2, 0, 0, 3, p2_task);
	process p3 (p3_n, 3, 0, 0, 3, p3_task);
	fcfs_scheduler f;
	f.insert (p1);
	f.insert (p2);
	f.insert (p3);
	f.schedule ();
	cout << "Avg waiting time (fcfs) : " << f.avg_waiting_time () << endl;
	cout << "Avg turnaround time (fcfs) : " << f.avg_turnaround_time () << endl;

	shortest_job_first s;
	s.insert (p1);
	s.insert (p2);
	s.insert (p3);
	s.schedule ();
	cout << "Avg waiting time (sjf) : " << s.avg_waiting_time () << endl;
	cout << "Avg turnaround time (sjf) : " << s.avg_turnaround_time () << endl;
 	return 0;
}
