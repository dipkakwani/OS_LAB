#include "process.h"
#ifndef SCH_H
#define SCH_H
using namespace std;

class scheduler
{
public:
	vector<process> ready_queue;
	virtual void schedule ()
	{
		for (auto p = ready_queue.begin (); p != ready_queue.end (); p++)
		{
			cout << "Executing " << (*p).name << endl;
			(*p).execute ();
		}
	}
	virtual void insert (const process& p) = 0;
	virtual double avg_waiting_time ()
	{
		double res = 0.0;
		unsigned long wait = 0;
		for (auto& p : ready_queue)
		{
			res += wait;
			wait += p.burst_time;
		}
		return res / ready_queue.size ();
	}
	virtual double avg_turnaround_time ()
	{
		double res = 0.0;
		unsigned long turn = 0;
		for (auto& p : ready_queue)
		{
			turn += p.burst_time;
			res += turn;
		}
		return res / ready_queue.size ();
	}

};
#endif
