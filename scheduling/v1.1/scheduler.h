#include "process.h"
using namespace std;
#ifndef SCH_H
#define SCH_H
#define timer_tick 1

/* Abstract class scheduler. */
class scheduler
{
protected:
  int timer_ticks;              // Current time elasped in timer_tick.
  vector<process> ready_queue;  // Processes ready to be scheduled.
  vector<process> job_queue;    // Maintains the list of jobs to be inserted in
                                // the ready queue; sorted by arrival time.
  process current_p;            // Process which is running currently.
  vector<int> waiting_time;
  vector<int> turn_around_time;

  /* Interrupt call after each timer_tick. Transfers all the processes from the
   * job_queue to ready_queue on their corresponding arrival times. */
  virtual void timer_interrupt ()
  {
    timer_ticks++;
    while (!job_queue.empty () && 
           job_queue.front ().arrival_time <= timer_ticks)
    {
      process p = job_queue.front ();
      job_queue.erase (job_queue.begin ());
      insert (p);
    }
  }

public:

  scheduler (const process& idle_process) : timer_ticks (-1), 
                                            current_p (idle_process)
  {}

  /* Order inserts process p in the job queue. timer_interrupt () is 
   * reponsible for moving it to the ready_queue on appropriate time. */
  void insert_job (const process& p)
  {
    waiting_time.push_back (0);
    turn_around_time.push_back (0);
    for (auto i = job_queue.begin (); i != job_queue.end (); i++)
    {
      if ((*i).arrival_time > p.arrival_time)
      {
        job_queue.insert (i, p);
        return;
      }
    }
    job_queue.push_back (p);
  }
  
  /* Inserts process p in the ready_queue. To be defined by the cpu scheduler.*/
  virtual void insert (const process& p) = 0;

  /* Picks up one of the process from the ready_queue to run. To be defined
   * by the cpu scheduler. */
  virtual void schedule () = 0;

  double avg_waiting_time ()
  {
    double res = 0.0;
    for (auto i : waiting_time)
      res += i;
    return res / waiting_time.size ();
  }

  double avg_turn_around_time ()
  {
    double res = 0.0;
    for (auto i : turn_around_time)
      res += i;
    return res / turn_around_time.size ();
  }

};
#endif
