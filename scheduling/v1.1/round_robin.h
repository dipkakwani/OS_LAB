#include "process.h"
#include "scheduler.h"
using namespace std;
#ifndef RR_H
#define RR_H
#define TIME_SLICE 2

class round_robin : public scheduler
{
public:
  round_robin (const process& idle_process) : scheduler:: 
                                              scheduler (idle_process)
  {}

	void insert (const process& p)
	{
    ready_queue.push_back (p);
	}
  
  void schedule ()
  {
    while (!job_queue.empty () || !ready_queue.empty ())
    {
      /* While there is no process to schedule, pass time. */
      while (ready_queue.empty ())
        timer_interrupt ();

      current_p = ready_queue.front ();
      ready_queue.erase (ready_queue.begin ());
      if (turn_around_time[current_p.pid - 1] == 0)
        waiting_time[current_p.pid - 1] = timer_ticks - current_p.arrival_time;
      else
        waiting_time[current_p.pid - 1] += timer_ticks
                                           - turn_around_time[current_p.pid - 1]
                                           - current_p.arrival_time;
      turn_around_time[current_p.pid - 1] = timer_ticks
                                            - current_p.arrival_time;
      
      current_p.execute ();
      int times = (TIME_SLICE > current_p.burst_time) ? TIME_SLICE
                                                       - current_p.burst_time
                                                       : TIME_SLICE;
      for (int i = 1; i <= times; i++)
      {
        turn_around_time[current_p.pid - 1]++;
        timer_interrupt ();
      }
      if (current_p.burst_time > TIME_SLICE)
      {
        current_p.burst_time -= TIME_SLICE;
        insert (current_p);
      }
    }
  }
};
#endif
