#include "scheduler.h"
#include "process.h"
#include "priority.h"
#include "round_robin.h"
using namespace std;

void idle_process_task ()
{
  while (1);
}
void p1_task ()
{
  cout << "Inside p1\n";
}
void p2_task ()
{
  cout << "Inside p2\n";
}
void p3_task ()
{
  cout << "Inside p3\n";
}
void p4_task ()
{
  cout << "Inside p4\n";
}

int main ()
{
  process idle_process ((string)"idle_process", 9999, INT_MIN, 0, 999999, 
                        &idle_process_task);
  process p1 ("p1", 1, 2, 0, 5, &p1_task);
  process p2 ("p2", 2, 3, 5, 6, &p2_task);
  process p3 ("p3", 3, 1, 3, 5, &p3_task);
  np_priority_scheduler npp (idle_process);
  npp.insert_job (p1);
  npp.insert_job (p2);
  npp.insert_job (p3);

  npp.schedule ();
  cout << "Avg waiting time : " << npp.avg_waiting_time () << endl;
  cout << "Avg turn around time : " << npp.avg_turn_around_time () << endl;

  p_priority_scheduler pp (idle_process);
  pp.insert_job (p1);
  pp.insert_job (p2);
  pp.insert_job (p3);

  pp.schedule ();
  cout << "Avg waiting time : " << pp.avg_waiting_time () << endl;
  cout << "Avg turn around time : " << pp.avg_turn_around_time () << endl;

  round_robin rr (idle_process);
  rr.insert_job (p1);
  rr.insert_job (p2);
  rr.insert_job (p3);

  rr.schedule ();
  cout << "Avg waiting time : " << rr.avg_waiting_time () << endl;
  cout << "Avg turn around time : " << rr.avg_turn_around_time () << endl;

/*
  process a ("A", 1, 3, 2, 5, &p1_task);
  process b ("B", 2, 4, 0, 4, &p2_task);
  process c ("C", 3, 1, 1, 3, &p3_task);
  process d ("D", 4, 2, 5, 4, &p4_task);

  np_priority_scheduler p_test1 (idle_process);
  p_test1.insert_job (a);
  p_test1.insert_job (b);
  p_test1.insert_job (c);
  p_test1.insert_job (d);

  p_test1.schedule ();
  cout << "Avg waiting time : " << p_test1.avg_waiting_time () << endl;
  cout << "Avg turn around time : " << p_test1.avg_turn_around_time () << endl;

  p_priority_scheduler p_test (idle_process);
  p_test.insert_job (a);
  p_test.insert_job (b);
  p_test.insert_job (c);
  p_test.insert_job (d);

  p_test.schedule ();
  cout << "Avg waiting time : " << p_test.avg_waiting_time () << endl;
  cout << "Avg turn around time : " << p_test.avg_turn_around_time () << endl;
  */
  return 0;
}
